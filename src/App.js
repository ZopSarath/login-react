import React, { useState, useEffect } from 'react'
import SignUp from './components/SignUp'
import LogIn from './components/LogIn'
import { StyledApp } from './StyledApp'

function App() {
  return (
  <StyledApp>
    <LogIn/>
    <SignUp/>
  </StyledApp>
  );
}

export default App;
