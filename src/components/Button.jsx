import React from 'react'
import {StyledButton} from './styles/StyledButton'
const Button = ({ handleButton, content }) => {
    return (
        <StyledButton onClick={handleButton}>{content}</StyledButton>
    )
}

export default Button
