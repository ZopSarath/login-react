import React, {useState, useEffect} from 'react'
import InputField from './InputField'
import Button from './Button'
import {StyledForm, StyledError, ErrorGap, SuccessMessage} from './styles/StyledForm'
import axios from 'axios'

const SignUp = () => {
  
    const [firstName, setFirstName] = useState('')
    const [errorFirstName, setErrorFirstName] = useState('')
    
    const [lastName, setLastName] = useState('')
    const [errorLastName, setErrorLastName] = useState('')
    
    const [email, setEmail] = useState('')
    const [errorEmail, setErrorEmail] = useState('')
   
    const [password, setPassword] = useState('')
    const [errorPassword, setErrorPassword] = useState('')

    const [confirmPassword, setConfirmPassword] = useState('')
    const [errorConfirmPassword, setErrorConfirmPassword] = useState('')

    const [dataEntered, setDataEntered] = useState(false)

    const handleFirstName = (e) => {
        setFirstName(e.target.value)
    }

    const handleLastName = (e) => {
        setLastName(e.target.value)
    }

    const handleEmail = (e) => {
        setEmail(e.target.value)
    }

    const handlePassword = (e) => {
        setPassword(e.target.value)
    }

    const handleConfirmPassword = (e) => {
        setConfirmPassword(e.target.value)
    }

    const handleSignUp = () => {
       const regEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

        if(firstName.length < 1) 
            setErrorFirstName('error')
        else
            setErrorFirstName('no-error')
        
        if(lastName.length < 1) 
            setErrorLastName('error')
        else
            setErrorLastName('no-error')
        
        if(!email.match(regEmail) || email.length < 1) 
            setErrorEmail('error')
        else
            setErrorEmail('no-error')
               
        if(password.length < 1)
            setErrorPassword('error')
        else
            setErrorPassword('no-error')

        if(confirmPassword.length < 1 || password !== confirmPassword)
            setErrorConfirmPassword('error')
        else
            setErrorConfirmPassword('no-error')
        
    }

    useEffect(() => {
        if(errorFirstName==='error'|| errorLastName==='error' || errorEmail==='error' || errorPassword === 'error' || errorConfirmPassword === 'error')
            setDataEntered(false)
        if(errorFirstName==='no-error' && errorLastName==='no-error' && errorEmail==='no-error' && errorPassword==='no-error' && errorConfirmPassword==='no-error')
            setDataEntered(true)
    },[handleSignUp])

    useEffect(async ()=>{
        const response = await axios.post('https://reqres.in/api/register',{
            "email": email,
            "password": password
        }).catch((e) => {})
        console.log(response)
    },[dataEntered])


    return (
        <>
            <StyledForm>
                <InputField handleField={handleFirstName} value={firstName} placeholder='First Name' />
                <ErrorGap>{(errorFirstName==='error') && <StyledError>First Name can't be that</StyledError>}</ErrorGap>
                <InputField handleField={handleLastName} value={lastName} placeholder='Last Name'/>
                <ErrorGap>{(errorLastName==='error') && <StyledError>Last Name can't be that</StyledError>}</ErrorGap>
                <InputField handleField={handleEmail} value={email} placeholder='Email'/>
                <ErrorGap>{(errorEmail==='error') && <StyledError>Email can't be that</StyledError>}</ErrorGap>
                <InputField handleField={handlePassword} value={password} placeholder='Password'/>
                <ErrorGap>{(errorPassword==='error') && <StyledError>Password can't be that</StyledError>}</ErrorGap>
                <InputField handleField={handleConfirmPassword} value={confirmPassword} placeholder='Confirm Password'/>
                <ErrorGap>{(errorConfirmPassword==='error') && <StyledError>Password doesn't match</StyledError>}</ErrorGap>
                <Button handleButton={handleSignUp} content='SignUp!'/>
                {dataEntered && <SuccessMessage>Registration Successful!</SuccessMessage>}
            </StyledForm>
            
        </> 
    )
}

export default SignUp
