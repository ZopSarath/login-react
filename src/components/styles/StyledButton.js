import styled from 'styled-components'

export const StyledButton = styled.button`
  background-color: hsl(154, 59%, 51%);
  font-size: 15px;
  box-shadow: 0px 3px 0px hsl(154, 58%, 42%);
  z-index: 2;
  height: 3rem;
  width: 15.5rem;
  border-radius: 3px;
  align-items: center;
  justify-content: center;
  text-align: center;
  border: none;
  color: rgb(255, 255, 255);
  &:hover{
      color: black;
      background-color: hsl(154, 86%, 61%);
  }
`