import styled from 'styled-components'

export const StyledInputField = styled.input`
    display:flex;
    height: 3rem;
    width: 15rem;
`
