import styled from 'styled-components'

export const StyledForm = styled.section`
    display:flex;
    justify-content:center;
    align-items:center;
    flex-direction:column;
    background:whitesmoke;
    padding: 2em;
    margin: 2em;
`

export const StyledError = styled.section`
    display:flex;
    text-align: center;
    color: red;
`

export const ErrorGap = styled.section`
    display:flex;
    height:1.5rem;
`
export const SuccessMessage = styled.section`
    display:flex;
    color: green;
    margin-top: 1em;
`

