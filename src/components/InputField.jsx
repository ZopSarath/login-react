import React, {useState} from 'react'
import {StyledInputField} from './styles/StyledInputField'

const InputField = ({handleField, value, placeholder}) => {
    return (
        <StyledInputField value={value}
        placeholder={placeholder}
        onChange={handleField} />
    )
}

export default InputField
