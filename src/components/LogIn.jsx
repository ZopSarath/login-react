import React, {useState, useEffect} from 'react'
import InputField from './InputField'
import Button from './Button'
import {StyledForm, StyledError, ErrorGap, SuccessMessage} from './styles/StyledForm'
import axios from 'axios'

function LogIn() {
    const [email, setEmail] = useState('')
    const [errorEmail, setErrorEmail] = useState('')
   
    const [password, setPassword] = useState('')
    const [errorPassword, setErrorPassword] = useState('')

    const [dataEntered, setDataEntered] = useState(false)

    const handleEmail = (e) => {
        setEmail(e.target.value)
    }

    const handlePassword = (e) => {
        setPassword(e.target.value)
    }

    const handleLogIn = () => {
        const regEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

         
         if(!email.match(regEmail) || email.length < 1) 
             setErrorEmail('error')
         else
             setErrorEmail('no-error')
                
         if(password.length < 1)
             setErrorPassword('error')
         else
             setErrorPassword('no-error')
         
     }

     useEffect(() => {
        if(errorEmail==='error' || errorPassword === 'error')
            setDataEntered(false)
        if(errorEmail==='no-error' && errorPassword==='no-error')
            setDataEntered(true)
    },[handleLogIn])

    useEffect(async ()=>{
        const response = await axios.post('https://reqres.in/api/login',{
            "email": email,
            "password": password
        }).catch((e) => {})
        console.log(response)
    },[dataEntered])

    return (
        <>
            <StyledForm>
                <InputField handleField={handleEmail} value={email} placeholder='Email'/>
                <ErrorGap>{(errorEmail==='error') && <StyledError>Email can't be that</StyledError>}</ErrorGap>
                <InputField handleField={handlePassword} value={password} placeholder='Password'/>
                <ErrorGap>{(errorPassword==='error') && <StyledError>Password can't be that</StyledError>}</ErrorGap>
                <Button handleButton={handleLogIn} content='LogIN!'/>
                {dataEntered && <SuccessMessage>success</SuccessMessage>}
            </StyledForm>
        </>
        
    )
}

export default LogIn
